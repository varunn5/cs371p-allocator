// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);}}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;}}
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);}
            x.deallocate(b, s);
            throw;}
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);}
        x.deallocate(b, s);}}

                                                               // uncomment
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);}                                         // fix test

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);}                                         // fix test

TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[0]));
    iterator check = iter++;
    ASSERT_EQ(*check, -7);
    ASSERT_EQ(*iter, 2815);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}

TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[3]));
    iterator check = iter++;
    ASSERT_EQ(*check, -8);
    ASSERT_EQ(*iter, 0);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}

TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[5]));
    iterator check = iter++;
    ASSERT_EQ(*check, 0);
    ASSERT_EQ(*iter, 0);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}

TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[6]));
    iterator check = iter++;
    ASSERT_EQ(*check, 0);
    ASSERT_EQ(*iter, 11);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}

TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-10,0,0,-8,10,0,1,0,11};
    iterator iter(&(arr[0]));
    iterator check = iter++;
    ASSERT_EQ(*check, -10);
    ASSERT_EQ(*iter, 0);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}


TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[1]));
    iterator check = iter++;
    ASSERT_EQ(*check, 0);
    ASSERT_EQ(*iter, -8);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}

TEST(AllocatorFixture, test10) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[2]));
    iterator check = iter++;
    ASSERT_EQ(*check, 0);
    ASSERT_EQ(*iter, 10);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}

TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<int, 1000>;
    using const_iterator       = typename allocator_type::const_iterator;
    const allocator_type x;
    ASSERT_EQ(reinterpret_cast<const int*>(&(*(x.begin()))), &(x[0]));
}

TEST(AllocatorFixture, test12) {
    using allocator_type = my_allocator<int, 1000>;
    using const_iterator       = typename allocator_type::const_iterator;
    const allocator_type x;
    ASSERT_EQ(reinterpret_cast<const int*>(&(*(x.end()))), &(x[1000])); // because: [996][997][998][999] <-- last char in a[]
}

TEST(AllocatorFixture, test13) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[5]));
    iterator check = iter--;
    ASSERT_EQ(*check, 0);
    ASSERT_EQ(*iter, 65535);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}

TEST(AllocatorFixture, test14) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[6]));
    iterator check = iter--;
    ASSERT_EQ(*check, 0);
    ASSERT_EQ(*iter, 10);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}

TEST(AllocatorFixture, test15) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[7]));
    iterator check = iter--;
    ASSERT_EQ(*check, 0);
    ASSERT_EQ(*iter, 0);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}

TEST(AllocatorFixture, test16) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[1]));
    iterator check = iter--;
    ASSERT_EQ(*check, 0);
    ASSERT_EQ(*iter, 0);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}

TEST(AllocatorFixture, test17) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-7,0,0,-8,10,0,0,0,11};
    iterator iter(&(arr[2]));
    iterator check = iter--;
    ASSERT_EQ(*check, 0);
    ASSERT_EQ(*iter, -7);
    *iter = 14;
    ASSERT_EQ(*iter, 14);
}
// non const allocator type
TEST(AllocatorFixture, test18) {
    using allocator_type = my_allocator<int, 1000>;
    using const_iterator       = typename allocator_type::const_iterator;
    allocator_type x;
    ASSERT_EQ(reinterpret_cast<const int*>(&(*(x.begin()))), &(x[0]));
}

TEST(AllocatorFixture, test19) {
    using allocator_type = my_allocator<int, 1000>;
    using const_iterator       = typename allocator_type::const_iterator;
    allocator_type x;
    ASSERT_EQ(reinterpret_cast<const int*>(&(*(x.end()))), &(x[1000])); // because: [996][997][998][999] <-- last char in a[]
}
