// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument


#include <iostream> // cin, cout
#include <sstream>  // istringstream
#include <string>   // getline, string
using namespace std;

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator
{
    // -----------
    // operator ==
    // -----------

    friend bool operator==(const my_allocator &, const my_allocator &)
    {
        return false;
    } // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator!=(const my_allocator &lhs, const my_allocator &rhs)
    {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using value_type = T;

    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using pointer = value_type *;
    using const_pointer = const value_type *;

    using reference = value_type &;
    using const_reference = const value_type &;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator
    {
        // -----------
        // operator ==
        // -----------

        friend bool operator==(const iterator &lhs, const iterator &rhs)
        {
            // <your code>
            return lhs._p == rhs._p;
        } // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator!=(const iterator &lhs, const iterator &rhs)
        {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int *_p;

    public:
        // -----------
        // constructor
        // -----------

        iterator(int *p)
        {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int &operator*() const
        {

            return *_p;
        } // replace!

        // -----------
        // operator ++
        // -----------

        iterator &operator++()
        {
            // <your code>
            char *block = (char *)_p;
            block += (*_p > 0) ? * _p : -1 * *_p;
            _p = (int *)block;
            // move 2 sentinels
            _p += 2;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator++(int)
        {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator &operator--()
        {
            // <your code>
            int block = *(reinterpret_cast<int *>(reinterpret_cast<char *>(_p) - 4));
            _p = reinterpret_cast<int *>(reinterpret_cast<char *>(_p) - abs(block) - 8);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator--(int)
        {
            iterator x = *this;
            --*this;
            return x;
        }
        friend class my_allocator;
        friend void allocator_solve(istream& sin, ostream& sout);
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator
    {
        // -----------
        // operator ==
        // -----------

         friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        } // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator!=(const const_iterator &lhs, const const_iterator &rhs)
        {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int *_p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator(const int *p)
        {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int &operator*() const
        {
            // <your code>
          return *_p;
        } // replace!

        // -----------
        // operator ++
        // -----------

        const_iterator &operator++()
        {
            // block
            int block = (*_p > 0) ? *_p : -1 * *_p;
            // two sentinels so 8
            int sent = 8;
             _p = reinterpret_cast<const int *>(reinterpret_cast<const char *>(_p) + block + sent);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator++(int)
        {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator &operator--()
        {
            // <your code>
            int fromRight = -4;
            // two sentinels so 8
            int sent = 8;
            int block = *(reinterpret_cast<const int *>(reinterpret_cast<const char *>(_p) + fromRight));
            block = (block > 0) ? block : -1 * block;
            _p = reinterpret_cast<const int *>(reinterpret_cast<const char *>(_p) - block - sent); // 2 sentinels so 8
            return *this;
        }
            // -----------
            // operator --
            // -----------

            const_iterator operator--(int)
            {
                const_iterator x = *this;
                --*this;
                return x;
            }
        
        };

    private:
        // ----
        // data
        // ----

        char a[N];

        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * Check sentinels for correctness and that no two free blocks are consecutively placed and right can be reached
         */
    bool valid () const {
        my_allocator::const_iterator right = const_iterator(&(*this)[N]);

        for( my_allocator::const_iterator left = const_iterator(&(*this)[0]); left != right; ++left ) { 
            my_allocator::const_iterator curr = left;
            ++curr;
            bool invalid = curr != right && *curr > 0 && *left > 0;
            if (invalid) { 
                return false;
                
            }
            --curr;
            invalid = curr != left;
            if(invalid) { 
                return false;
            }
        }
        return true;
    }


    public:
        // -----------
        // constructor
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator()
        {
            (*this)[0] = N - 8;
            (*this)[N - 4] = N - 8;
            assert(valid());
        }

        my_allocator(const my_allocator &) = default;
        ~my_allocator() = default;
        my_allocator &operator=(const my_allocator &) = default;

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
        pointer allocate(size_type num)
        {
            if (num <= 0)
            {
                throw std::bad_alloc();
            }

            iterator iter = iterator(&(*this)[0]);
            int num_bytes = num * sizeof(T);
            int ind = 0;
            int sent_size = sizeof(int);

            while (*iter < (num_bytes) && *iter != 0)
            {
                int add = *iter;
                add = (add < 0) ? add * -1 : add;
                add += 2 * sent_size;
                ind += add;
                ++iter;
            }

            if (*iter == 0)
            {
                throw std::bad_alloc();
            }

            int prev = *iter;
            int news = *iter;
            news -= num_bytes;
            news -= 2 * sent_size;

            int check = sizeof(T);
            bool enough = news < check;
            if (enough)
            {
                (*this)[ind] = prev * -1;
                int thisind = ind;
                thisind += prev;
                thisind += sent_size;
                (*this)[thisind] = prev * -1;
                assert(valid());
                thisind -= prev;
                return (pointer)(&(*this)[ind + sent_size]);
            }
            else
            {
                (*this)[ind] = num_bytes * -1;
                int mem_index = ind;
                mem_index += num_bytes;
                mem_index += sent_size;
                (*this)[mem_index] = num_bytes * -1;
                mem_index += sent_size;
                (*this)[mem_index] = news;
                mem_index = ind;
                mem_index += prev + sent_size;
                (*this)[mem_index] = news;

                assert(valid());
                return (pointer)(&(*this)[ind + sent_size]);
            }
        }

        // ---------
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct(pointer p, const_reference v)
        {
            new (p) T(v); // this is correct and exempt
            assert(valid());
        } // from the prohibition of new

        // ----------
        // deallocate
        // ----------

        /**
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         * <your documentation>
         */
        //if pointer is not within range of array, throw exception
     void deallocate(pointer p, size_type)
    {

        if (p < (pointer)&a[0] || p >= (pointer)&a[N - 1])
        {
            throw std::invalid_argument("invalid pointer");
        }
        int sent_size = sizeof(int);
        //set left and right sentinel
        pointer left = (pointer)((char *)p - sent_size);
        int *leftsent = reinterpret_cast<int *>(left);
        *leftsent = *leftsent * -1;
        pointer right = (pointer)((char *)p + *leftsent);
        int *rightsent = reinterpret_cast<int *>(right);
        *rightsent = *rightsent * -1;
        bool match = *leftsent == *rightsent;
        //throw exception
        if (!match)
        {
            throw std::invalid_argument("invalid pointer");
        }

        pointer bottom = (pointer)((char *)left - sent_size);
        bool inRange = bottom >= (pointer)&a[0];
        //be in range
        if (inRange)
        {

            int *right = reinterpret_cast<int *>(bottom);

            //merge 
            if (*right > 0)
            {
                pointer bot = (pointer)((char *)bottom - *right - sent_size);
                int *bbotsent = reinterpret_cast<int *>(bot);
                int mem = *bbotsent; 
                mem += *leftsent;
                mem += (2 * sent_size);
                *rightsent = mem;
                *bbotsent = mem;
                left = bot;
                leftsent = reinterpret_cast<int *>(left);
            }
        }

  
        pointer top = (pointer)((char *)right + sent_size);
        inRange = top < (pointer)&a[N - 1];
        //be in range
        if (inRange)
        {
            int *sent = reinterpret_cast<int *>(top);

            // 
            if (*sent > 0)
            {
                pointer ttop = (pointer)((char *)top + *sent + sent_size);
                int *rsent = reinterpret_cast<int *>(ttop);
                int mem = *rsent;
                mem += *leftsent;
                mem += (2 * sent_size);
                *rsent = mem;
                *leftsent = mem;
            }
        }

        assert(valid());
    }
    // -------
    // destroy
    // -------

    /**
         * O(1) in space
         * O(1) in time
         */
    void destroy(pointer p)
    {
        p->~T(); // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------
    /**
         * O(1) in space
         * O(1) in time
         */
    int &operator[](int i)
    {
        return *reinterpret_cast<int *>(&a[i]);
    }

    /**
         * O(1) in space
         * O(1) in time
         */
    const int &operator[](int i) const
    {
        return *reinterpret_cast<const int *>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
         * O(1) in space
         * O(1) in time
         */
    iterator begin()
    {
        return iterator(&(*this)[0]);
    }

    /**
         * O(1) in space
         * O(1) in time
         */
    const_iterator begin() const
    {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
         * O(1) in space
         * O(1) in time
         */
    iterator end()
    {
        return iterator(&(*this)[N]);
    }

    /**
         * O(1) in space
         * O(1) in time
         */
    const_iterator end() const
    {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
