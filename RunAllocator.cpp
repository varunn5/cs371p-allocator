// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"
#include <sstream>  // istringstream
#include <string>   // getline, string


using namespace std;
// ----
// main
// ----




void allocator_solve (istream& sin, ostream& sout) {
        using allocator_type = my_allocator<double, 1000>; 
    using value_type     = typename allocator_type::value_type;
    using pointer        = typename allocator_type::pointer;
    using iterator       = typename allocator_type::iterator;

    string s;
    string str;
    getline(sin, str);
    stringstream stream(str);
    int tests;
    stream >> tests; 
 
    getline(sin, s); // blank line

    while (tests > 0) {
        allocator_type alloc;
        while(getline(sin, s) && s.length() != 0) {
            int number;
            istringstream sin(s); 
            sin >> number;
            assert(number != 0);
            if (number > 0) {
                const pointer begin = alloc.allocate(number); 
                pointer ptr = begin;
                int j = number;
                while (j > 0) {
                    alloc.construct(ptr, 0.0);
                    ++ptr;
                    j--;
                }
            }
            else { 
                iterator begin = alloc.begin();
                int count = 0;

                while (count != abs(number)) {
                    if (*begin < 0) { 
                        ++count;
                    }
                    if (count != abs(number)) { 
                        ++begin;
                    }
                }
                int elem = abs((*begin) / ((int)sizeof(value_type))); 
                value_type* arr = reinterpret_cast<value_type*>(reinterpret_cast<char*>(begin._p) + 4); 
                int j = elem;
                while( j > 0) {
                    alloc.destroy(&(arr[j]));
                    j--;
                }
                alloc.deallocate((pointer)(reinterpret_cast<char*>(begin._p) + 4), elem); 
            }

        }
        iterator right = alloc.end();
        bool printed = false;

        for (iterator left = alloc.begin(); left != right; ++left) {
            if (printed) {
                sout << " "; 
            }
            sout << *left;
            printed = true;
        }
        sout << "\n";
        tests--;
    }


}

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */
    allocator_solve(cin, cout);

    return 0;
}
